
platform项目
SSF\code\platform\src\main\resources 
添加core-site.xml和hdfs-site.xml文件
SSF\code\platform\src\main\resources\application.properties 
参考注释配置相关参数，重要几个参数一下解释一下
upfile.path 附件上传的临时目录,包括job和jar,config文件
SysSet.sparkApplication 在hdfs上创建该目录，并给予SysSet.hdfsAccount权限
SysSet.livy Livy 提交spark streaming的地址
SysSet.intervalTime 每个多久调度一次任务
SysSet.yarn.web yarn的webui地址
SysSet.taskChart.intervalTime统计图表每个多久获取一次数据
app.logs.path 日志存放路径
yarn.logs.download.shellscript 执行yarn logs拉取日志的脚本存放地址,ssh文件存放于SSF\code\platform\src\main\java\bigdata\java\platform\file\buildsparklog.sh
kafkaparam.bootstrap.servers kafka broker list
hbaseparam.hbase.zookeeper.quorum 使用hbase是配置zookeeper的地址
hbaseparam.hbase.zookeeper.property.clientPort zookeeper端口
hbaseparam.zookeeper.znode.parent hbase在zookeeper的节点名称 
短信发送接口：
bigdata.java.platform.util.Comm.java下的sendMsg方法，根据自己的短信接口进行修改。

framework项目
使用maven命令install到本地仓库即可。

bigdata项目
SSF\code\bigdata\spark\src\main\resources\application.properties 
参考注释配置相关参数，重要几个参数一下解释一下
sparkconf.spark.master 在开发环境中使用，线上环境请注释这个
kafkaparam.bootstrap.servers kafka broker list
KafkaOffsetPersist=MySql kafka偏移量保存介质()
StopByMark=MySql 优雅关闭信息保存介质
SSF\code\bigdata\spark\src\main\resources\c3p0-config.xml
named-config name="JdbcMysql"中的配置数据库和platform项目中application.properties链接mysql的数据库保持一致即可。

platform打包war文件，并上传到tomcat中，执行mysql元数据脚本（SSF\code\platform\src\main\java\bigdata\java\platform\file\platform.sql），管理后台地址http://ip:port/platform/login初始化账号密码admin/111111