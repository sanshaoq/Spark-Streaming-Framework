/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.redis;

import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class JedisConnectionPool {

    private JedisPool jedisPool = null;
    JedisConfig jedisConfig;
    public JedisConnectionPool(JedisConfig jedisConfig)
    {
        this.jedisConfig = jedisConfig;
        initPool();
    }

    void initPool()
    {
        if(StringUtils.isBlank(jedisConfig.getPassword()))
        {
            jedisPool = new JedisPool(jedisConfig, jedisConfig.getServer(), jedisConfig.getPort(), 10000);
        }
        else
        {
            jedisPool = new JedisPool(jedisConfig, jedisConfig.getServer(), jedisConfig.getPort(), 10000,jedisConfig.getPassword());
        }
    }

    public Jedis getConnection() {
        return jedisPool.getResource();
    }

    public void returnConnection(Jedis jedis) {
        jedis.close();
    }

}
