/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util.client;

import bigdata.java.framework.spark.pool.hbase.HbaseConfig;
import bigdata.java.framework.spark.pool.hbase.HbaseConnectionPool;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.*;

/**
 * Hbase客户端工具类
 */
public class HBaseClient {
    private static HBaseClient instance=null;
    public static HBaseClient getInstance(){
        if(instance == null){
            synchronized(HBaseClient.class){
                if(instance==null){
                    instance = new HBaseClient();
                }
            }
        }
        return instance;
    }
    HbaseConnectionPool hbaseConnectionPool;
    private HBaseClient()
    {
//        System.setProperty("hadoop.home.dir", "D:\\hadoop-common-2.2.0-bin-master");
        System.setProperty("HADOOP_USER_NAME", "yxgk");
        HbaseConfig hbaseConfig = new HbaseConfig();
        hbaseConnectionPool = new HbaseConnectionPool(hbaseConfig);
    }

    /**
     * 获取yxgk表名
     * @param tabelName 添加yxgk名称空间后的表名
     * @return yxgk:tableName
     */
    public static String getTableName(String tabelName)
    {
        return TABLEPREFIX + tabelName;
    }

    /**
     * 默认列族f
     */
    public static final String CF_DEFAULT = "f";

    /**
     * 默认列
     */
    public static final String JSON = "json";
    private static final String  TABLEPREFIX = "yxgk:";

    public HbaseConnectionPool getConnectionPool() {
        return hbaseConnectionPool;
    }

    /**
     * 归还链接
     * @param pool 连接池对象
     * @param resource 连接对象
     */
    public static void returnConnection(HbaseConnectionPool pool, Connection resource,Table table){
        if (null != table) {
            try {
                table.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if(pool!=null && resource!=null)
        {
            pool.returnConnection(resource);
        }
    }

    /**
     * 归还链接
     * @param pool 连接池对象
     * @param resource 连接对象
     */
    public static void returnConnection(HbaseConnectionPool pool, Connection resource,Admin admin){
        if(admin!=null)
        {
            try {
                admin.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if(pool!=null && resource!=null)
        {
            pool.returnConnection(resource);
        }
    }

    /**
     * 根据rowkey判断数据是否存在
     * @param tableName 表名
     * @param rowKey rowkey
     * @return true存在，false不存在
     */
    public Boolean exists(String tableName, String rowKey) {
        Table table = null;
        Boolean exists = false;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table =resource.getTable(TableName.valueOf(tableName));
            Get get = new Get(rowKey.getBytes());
            exists = table.exists(get);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return exists;
    }

    /**
     * 往hbase中插入一条数据
     * @param tableName 表名
     * @param rowKey rowkey
     * @param columnFamily 列族
     * @param column 列名
     * @param value value内容
     */
    public void put(String tableName, String rowKey, String columnFamily, String column, String value) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Put put = new Put(rowKey.getBytes());//rowkey
            put.addColumn(columnFamily.getBytes(), column.getBytes(), String.valueOf(value).getBytes());
            table.put(put);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    /**
     * 往hbase中插入一条数据(默认列族f，默认列json)
     * @param tableName 表名
     * @param rowKey rowkey
     * @param value 列名
     */
    public void putCfDefault(String tableName, String rowKey, String value) {
        put(tableName,rowKey,CF_DEFAULT,JSON,value);
    }

    /**
     * 不存在则创建名称空间
     * @param nameSpace 名称空间
     */
    public void createNamespaceNotExists(String nameSpace)
    {
        if(!namespaceisExists(nameSpace))
        {
            createNamespace(nameSpace);
        }
    }

    /**
     * 创建名称空间
     * @param nameSpace 名称空间
     */
    public void createNamespace(String nameSpace)
    {
        Admin admin = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            admin = resource.getAdmin();
            NamespaceDescriptor namespaceDescriptor = NamespaceDescriptor.create(nameSpace).build();
            admin.createNamespace(namespaceDescriptor);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource,admin);
        }
    }

    /**
     * 判断名称空间是否存在
     * @param nameSpace 名称空间
     * @return true存在，false不存在
     */
    public Boolean namespaceisExists(String nameSpace)
    {
        Admin admin = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            admin = resource.getAdmin();
            NamespaceDescriptor namespaceDescriptor=null;
            try
            {
                namespaceDescriptor = admin.getNamespaceDescriptor(nameSpace);
            }
            catch (NamespaceNotFoundException e)
            {
                return false;
            }

            if(namespaceDescriptor ==null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource,admin);
        }
    }

    /**
     * 列出名称空间下所有表名
     * @param nameSpace 名称空间
     * @return 表名
     */
    public List<String> listTableNamesByNamespace(String nameSpace)
    {
        Admin admin = null;
        List<String> list = new ArrayList<>();
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            admin = resource.getAdmin();
            TableName[] tableNames = admin.listTableNamesByNamespace(nameSpace);
            if(tableNames==null || tableNames.length < 1)
            {
                return list;
            }
            for (int i = 0; i < tableNames.length; i++) {
                list.add(tableNames[i].getNameAsString());
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource,admin);
        }
        return list;
    }

    /**
     * 批量插入数据
     * @param tableName 表名
     * @param putList put list对象
     */
    public void putList(String tableName, List<Put> putList)
    {
        if(putList.size() < 1)
        {
            return;
        }
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            table.put(putList);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    /**
     * 批量插入数据
     * @param tableName 表名
     * @param keyName key的名称
     * @param columnAndValues map list数据
     */
    public void put(String tableName, String keyName,List<Map<String, String>> columnAndValues) {
        put(tableName,keyName,"f",columnAndValues);
    }

    /**
     * 批量插入数据
     * @param tableName 表名
     * @param columnAndValues map list数据
     */
    public void put(String tableName,List<Map<String, String>> columnAndValues) {
        put(tableName,"rowkey","f",columnAndValues);
    }

    /**
     * 批量插入数据
     * @param tableName 表名
     * @param keyName key的名称
     * @param columnFamily 列族
     * @param columnAndValues map list数据
     */
    public void put(String tableName, String keyName,String columnFamily, List<Map<String, String>> columnAndValues) {

        if(columnAndValues.size() < 1)
        {
            return;
        }

        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            List<Put> putList = new ArrayList<>();
            for (int i = 0; i < columnAndValues.size(); i++) {
                Map<String, String> map = columnAndValues.get(i);
                String rowkey = map.get(keyName);
                Put put = new Put(rowkey.getBytes());//rowkey
                for (Map.Entry<String,String> entry: map.entrySet()) {
                    String column = entry.getKey();
                    String value = entry.getValue();
                    if(!column.equals(keyName))
                    {
                        put.addColumn(columnFamily.getBytes(),column.getBytes(),String.valueOf(value).getBytes());
                    }
                }
                putList.add(put);
            }
            table.put(putList);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    /**
     * 插入单条数据
     * @param tableName 表名
     * @param rowKey rowkey
     * @param columnFamily 列族
     * @param columnAndValues 列及对应内容
     */
    public void put(String tableName, String rowKey, String columnFamily, Map<String, String> columnAndValues) {
        if(columnAndValues.size() < 1)
        {
            return;
        }
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Put put = new Put(rowKey.getBytes());//rowkey
            for (Map.Entry<String, String> entry : columnAndValues.entrySet()) {
                String column = entry.getKey();
                String value = entry.getValue();
                put.addColumn(columnFamily.getBytes(), column.getBytes(), String.valueOf(value).getBytes());
            }
            table.put(put);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }
    /**
     * hbase计数器操作
     * @param tableName 表名
     * @param rowKey rowkey
     * @param columnFamily 列族
     * @param column 列名
     * @param count 数值
     * @return 计数后结果
     */
    public Long incrementColumnValue(String tableName
            , String rowKey
            , String columnFamily
            , String column
            , Long count) {
        Long counter = null;
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            counter = table.incrementColumnValue(Bytes.toBytes(rowKey), Bytes.toBytes(columnFamily), Bytes.toBytes(column), count);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return counter;
    }

    /**
     * 根据rowkey获取多列数据
     * @param tableName 表名
     * @param rowKey rowkey
     * @return Result对象
     */
    public Result get(String tableName, String rowKey) {
        Result result = null;
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            Get get = new Get(Bytes.toBytes(rowKey));
            table = resource.getTable(TableName.valueOf(tableName));
            result = table.get(get);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return result;
    }

    /**
     * 根据多个rowkey批量获取多列数据
     * @param tableName 表名
     * @param rowKeyList rowkey list
     * @param columnFamily 列族
     * @param columnsName 列名
     * @return map list
     */
    public List<Map<String,String>> get(String tableName, List<String> rowKeyList, String columnFamily,String[] columnsName) {
        return get(tableName,rowKeyList,"rowkey",columnFamily,columnsName);
    }

    /**
     * 根据多个rowkey批量获取多列数据
     * @param tableName 表名
     * @param rowKeyList rowkey list
     * @return map list
     */
    public List<Map<String,String>> get(String tableName, List<String> rowKeyList) {
        return get(tableName,rowKeyList,"f",new String[]{"json"});
    }

    /**
     * 根据多个rowkey批量获取多列数据
     * @param tableName 表名
     * @param rowKeyList rowkey list
     * @param rowKeyName rowkey字段名称
     * @param columnFamily 列族
     * @param columnsName 列名
     * @return map list
     */
    public List<Map<String,String>> get(String tableName, List<String> rowKeyList,String rowKeyName, String columnFamily,String[] columnsName) {

        if(rowKeyList.size() < 1)
        {
            return new ArrayList<>();
        }

        for (int k = 0; k < columnsName.length; k++) {
            String s = columnsName[k];
            if(s.toLowerCase().equals(rowKeyName.toLowerCase()))
            {
                throw new RuntimeException("rowKeyName '"+rowKeyName+"' and columnsName '"+s+"' duplicate");
            }
        }

        List<Map<String,String>> list = new ArrayList<>();
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            List<Get> getList = new ArrayList<>();
            for (int i = 0; i < rowKeyList.size(); i++) {
                String rowKey= rowKeyList.get(i);
                Get get = new Get(Bytes.toBytes(rowKey));
                for (int j = 0; j < columnsName.length; j++) {
                    get.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnsName[j]));
                }
                getList.add(get);
            }
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Result[] results = table.get(getList);
            for (int i = 0; i < results.length; i++) {
                Result r = results[i];
                Map<String,String> map = new HashMap<>();

                if(r.getRow() ==null || r.getRow().length < 1)
                {
                    continue;
                }

                map.put(rowKeyName,new String(r.getRow()));
                KeyValue[] kv =r.raw();
                for (int k = 0; k < kv.length; k++) {
//                    String qualifier = new String(kv[k].getQualifier());
//                    if(qualifier.toLowerCase().equals(rowKeyName.toLowerCase()))
//                    {
//                        throw new RuntimeException("rowKeyName '"+rowKeyName+"' on qualifier column name duplicate");
//                    }
                    map.put(new String(kv[k].getQualifier()),new String(kv[k].getValue()));
                }
                list.add(map);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return list;
    }

    /**
     * 根据rowkey返回多列及内容
     * @param tableName 表名
     * @param rowKey rowkey
     * @param columnFamily 列族
     * @param columnsName 列名
     * @return map列和value值
     */
    public Map<String,String> get(String tableName, String rowKey, String columnFamily,String[] columnsName) {
        Result result = null;
        Map<String,String> map = new HashMap<>();
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            Get get = new Get(Bytes.toBytes(rowKey));
            for (int i = 0; i < columnsName.length; i++) {
                get.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnsName[i]));
            }
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            result = table.get(get);
            KeyValue[] kv =result.raw();
            for (int i = 0; i < kv.length; i++) {
                map.put(new String(kv[i].getQualifier()),new String(kv[i].getValue()));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return map;
    }

    /**
     * 根据rowkey返回单列内容(默认列族f，默认列json)
     * @param tableName 表名
     * @param rowKey rowkey
     */
    public String getCfDefault(String tableName, String rowKey) {
        return  get(tableName,rowKey,CF_DEFAULT,JSON);
    }

    /**
     * 根据rowkey返回单列内容
     * @param tableName 表名
     * @param rowKey rowkey
     * @param columnFamily 列族
     * @param columnName 列名
     */
    public String get(String tableName, String rowKey, String columnFamily, String columnName) {
        if(StringUtils.isBlank(rowKey))
        {
            return null;
        }
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        Result result = null;
        String value = "";
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Get get = new Get(Bytes.toBytes(rowKey));
            get.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName));
//            boolean isCheckExistenceOnly = get.isCheckExistenceOnly();
            result = table.get(get);
            byte[] resByte = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName));
            value = Bytes.toString(resByte);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return value;
    }

    /**
     * 如果表名不存在则创建该表
     * @param tableName 表名
     * @param ColumnFamilys 列族
     */
    public void createTableNotExists(String tableName, String... ColumnFamilys) {
        Admin admin = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            admin = resource.getAdmin();
            if (admin.tableExists(TableName.valueOf(tableName))) {
            } else {
                HTableDescriptor table = new HTableDescriptor(TableName.valueOf(tableName));
                for (String family : ColumnFamilys) {
                    HColumnDescriptor columnfamily = new HColumnDescriptor(family);
                    table.addFamily(columnfamily);
                }
                admin.createTable(table);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,admin);
        }
    }

    /**
     * 如果表名不存在则创建该表
     * @param tableName 表名
     * @param splitKeys 分裂的key范围
     * @param ColumnFamilys 列族
     */
    public void createTableNotExistsBySplit(String tableName,byte[][] splitKeys, String... ColumnFamilys) {
        Admin admin = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            admin = resource.getAdmin();
            if (admin.tableExists(TableName.valueOf(tableName))) {
            } else {
                HTableDescriptor table = new HTableDescriptor(TableName.valueOf(tableName));
                for (String family : ColumnFamilys) {
                    HColumnDescriptor columnfamily = new HColumnDescriptor(family);
                    table.addFamily(columnfamily);
                }
                admin.createTable(table,splitKeys);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource,admin);
        }
    }

    /**
     * 批量删除数据
     * @param tableName 表名
     * @param rowKeyList rowkey list
     */
    public void deleteRows(String tableName, List<String> rowKeyList) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            List<Delete> deleteList = new ArrayList<>();
            table = resource.getTable(TableName.valueOf(tableName));
            for (int i = 0; i < rowKeyList.size(); i++) {
                String rowKey = rowKeyList.get(i);
                Delete delete = new Delete(Bytes.toBytes(rowKey));
                deleteList.add(delete);
            }
            table.delete(deleteList);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    /**
     * 删除单条数据
     * @param tableName 表名
     * @param rowKey rowkey
     */
    public void deleteRow(String tableName, String rowKey) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Delete delete = new Delete(Bytes.toBytes(rowKey));
            table.delete(delete);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    /**
     * 根据rowkey删除单列数据
     * @param tableName 表名
     * @param rowKey rowkey
     * @param columnFamily 列族
     * @param column 列名
     */
    public void deleteCol(String tableName, String rowKey, String columnFamily, String column) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Delete delete = new Delete(Bytes.toBytes(rowKey));
            delete.deleteColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(column));
            table.delete(delete);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    /**
     * 根据rowkey删除指定版本
     * @param tableName 表名
     * @param rowKey rowkey
     * @param columnFamily 列族
     * @param timestamp 时间版本
     */
    public void deleteVersion(String tableName, String rowKey, String columnFamily, Long timestamp) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Delete delete = new Delete(Bytes.toBytes(rowKey));
            delete.deleteFamilyVersion(Bytes.toBytes(columnFamily), timestamp);
            table.delete(delete);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    /**
     * 删除列族
     * @param tableName 表名
     * @param columnFamily 列族
     */
    public void deleteFamily(String tableName, String columnFamily) {
        Admin admin = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            admin = resource.getAdmin();
            admin.disableTable(TableName.valueOf(tableName));
            HTableDescriptor table = admin.getTableDescriptor(TableName.valueOf(tableName));
            table.removeFamily(Bytes.toBytes(columnFamily));
            admin.modifyTable(TableName.valueOf(tableName), table);
            admin.enableTable(TableName.valueOf(tableName));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,admin);
        }
    }

    /**
     * 删除指定的表
     * @param tableName 表名
     */
    public void dropTable(String tableName) {
        Admin admin = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            admin = resource.getAdmin();
            TableName tb = TableName.valueOf(tableName);
            if(admin.tableExists(tb))
            {
                if(admin.isTableEnabled(tb))
                {
                    admin.disableTable(tb);
                }
                admin.deleteTable(tb);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,admin);
        }
    }

    /**
     * 全表扫描（慎用）
     * @param tableName 表名
     * @return ResultScanner
     */
    public ResultScanner scanTable(String tableName) {
        Table table = null;
        ResultScanner rs = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            Scan scan = new Scan();
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            rs = table.getScanner(scan);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            returnConnection(pool,resource,table);
        }
        return rs;
    }

    /**
     * 全表扫描（慎用）
     * @param tableName 表名
     */
    public List<Map<String,String>> scan(String tableName) {
        return scan(tableName,null);
    }
    /**
     * 全表扫描（慎用）
     * @param tableName 表名
     * @param filter 过滤器
     */
    public List<Map<String,String>> scan(String tableName,Filter filter) {
        Table table = null;
        ResultScanner rs = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        List<Map<String,String>> list = new ArrayList<>();
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Scan s = new Scan();
            if(filter!=null)
            {
                s.setFilter(filter);
            }
            rs = table.getScanner(s);
            for (Result r : rs) {
                KeyValue[] kv = r.raw();
                Map<String,String> map = new HashMap<>();
                map.put("rowkey",new String(kv[0].getRow()));
                for (int i = 0; i < kv.length; i++) {
                    map.put(new String(kv[i].getFamily())+":"+new String(kv[i].getQualifier()),new String(kv[i].getValue()));
                }
                list.add(map);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            returnConnection(pool,resource,table);
        }
        return list;
    }

    public String getColumnByRowKey(String tableName, String rk, String colum) {
        Table table = null;
        Result result = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            Get get = new Get(Bytes.toBytes(rk));
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            result = table.get(get);
            CellScanner cellScanner = result.cellScanner();
            while (cellScanner.advance()) {
                Cell current = cellScanner.current();
                //获取value值
                String valueStr = new String(current.getValueArray(), current.getValueOffset(), current.getValueLength());
                JSONObject jsonObject = JSONObject.parseObject(valueStr);
                String col = jsonObject.get(colum).toString();
                return col;
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return "";
    }

    public Boolean isContainByRowFilter(String tableName, String str) {
        Table table = null;
        ResultScanner rs = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Scan s = new Scan();
            Filter filter = new RowFilter(CompareFilter.CompareOp.EQUAL, new SubstringComparator(str));
            s.setFilter(filter);
            rs = table.getScanner(s);
            Iterator<Result> iterator = rs.iterator();
            if(iterator.hasNext()){
                return true;
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }

            returnConnection(pool,resource,table);
        }
        return false;
    }

    public ResultScanner scanByRowFilter(String tableName, String str) {
        Table table = null;
        ResultScanner rs = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Scan s = new Scan();
            Filter filter = new RowFilter(CompareFilter.CompareOp.EQUAL, new SubstringComparator(str));
            s.setFilter(filter);
            rs = table.getScanner(s);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            returnConnection(pool,resource,table);
        }
        return rs;
    }

    public Set<String> getRowkeyByRowFilter(String tableName, String str) {
        Table table = null;
        ResultScanner rs = null;
        Set<String> rkList = new HashSet<String>();
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Scan s = new Scan();
            Filter filter = new RowFilter(CompareFilter.CompareOp.EQUAL, new SubstringComparator(str));
            s.setFilter(filter);
            rs = table.getScanner(s);
            Iterator<Result> iter = rs.iterator();
            while (iter.hasNext()) {
                Result result = iter.next();
                CellScanner cellScanner = result.cellScanner();
                while (cellScanner.advance()) {
                    Cell current = cellScanner.current();
                    //获取行键
                    byte[] rowArray = current.getRowArray();
                    String rowStr = new String(rowArray, current.getRowOffset(), current.getRowLength());
                    rkList.add(rowStr);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            returnConnection(pool,resource,table);
        }
        return rkList;
    }

    public List<String[]> scanByPrefixFilter(String tableName, String rowPrifix) {
        Table table = null;
        ResultScanner rs = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        List<String[]> list = new ArrayList<>();
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Scan s = new Scan();
            s.setFilter(new PrefixFilter(rowPrifix.getBytes()));
            rs = table.getScanner(s);
            for (Result r : rs) {
                KeyValue[] kv = r.raw();
                for (int i = 0; i < kv.length; i++) {
                    String[] strings = new String[]{
                            new String(kv[i].getRow())
                            ,new String(kv[i].getFamily())
                            ,new String(kv[i].getQualifier())
                            ,new String(kv[i].getValue())
                            ,kv[i].getTimestamp() + ""
                    };
                    list.add(strings);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            returnConnection(pool,resource,table);
        }
        return list;
    }

    public void scanRow(String tableName, String rowKey) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            Get get = new Get(Bytes.toBytes(rowKey));
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Result result = table.get(get);
            for (KeyValue kv : result.list()) {
                System.out.println(
                        rowKey + "    column=" + Bytes.toString(kv.getFamily()) + ":" + Bytes.toString(kv.getQualifier())
                                + "," + "timestamp=" + kv.getTimestamp() + ",value=" + Bytes.toString(kv.getValue()));
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    public Boolean isContainByRowKey(String tableName, String rowKey) {
        Result result = null;
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            Get get = new Get(Bytes.toBytes(rowKey));
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            result = table.get(get);
            if (result.advance()) {
                return true;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
        return false;
    }

    public void scanSpecifyColumn(String tableName, String rowKey, String columnFamily, String column) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Get get = new Get(Bytes.toBytes(rowKey));
            get.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(column));
            Result result = table.get(get);
            for (KeyValue kv : result.list()) {
                System.out.println(
                        rowKey + "    column=" + Bytes.toString(kv.getFamily()) + ":" + Bytes.toString(kv.getQualifier())
                                + "," + "timestamp=" + kv.getTimestamp() + ",value=" + Bytes.toString(kv.getValue()));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    public void scanSpecifyTimestamp(String tableName, String rowKey, Long timestamp) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            Get get = new Get(Bytes.toBytes(rowKey));
            get.setTimeStamp(timestamp);
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Result result = table.get(get);
            for (KeyValue kv : result.list()) {
                System.out.println(
                        rowKey + "    column=" + Bytes.toString(kv.getFamily()) + ":" + Bytes.toString(kv.getQualifier())
                                + "," + "timestamp=" + kv.getTimestamp() + ",value=" + Bytes.toString(kv.getValue()));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    public void scanAllVersion(String tableName, String rowKey) {
        Table table = null;
        HbaseConnectionPool pool = null;
        Connection resource = null;
        try {
            Get get = new Get(Bytes.toBytes(rowKey));
            get.setMaxVersions();
            pool = getConnectionPool();
            resource = pool.getConnection();
            table = resource.getTable(TableName.valueOf(tableName));
            Result result = table.get(get);
            for (KeyValue kv : result.list()) {
                System.out.println(
                        rowKey + "    column=" + Bytes.toString(kv.getFamily()) + ":" + Bytes.toString(kv.getQualifier())
                                + "," + "timestamp=" + kv.getTimestamp() + ",value=" + Bytes.toString(kv.getValue()));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            returnConnection(pool,resource,table);
        }
    }

    public static void main(String[] args) {
        List<Map<String,String>> scan = HBaseClient.getInstance().scan("yxgk:o_org");
    }
}
