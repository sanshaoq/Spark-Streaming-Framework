/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.MySqlUtil;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.spark.streaming.kafka010.OffsetRange;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class KafkaOffsetPersistForMySql extends KafkaOffsetPersist {
    String tableName ="platform.KAFKAOFFSET";
    FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");
    public KafkaOffsetPersistForMySql(String appName, String topic, String groupId) {
        super(appName, topic, groupId);
//        String createSql = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (" +
//                "  `APPNAME` varchar(100) NOT NULL," +
//                "  `TOPIC` varchar(100) NOT NULL," +
//                "  `GROUPID` varchar(100) NOT NULL," +
//                "  `PARTITION` int(255) NOT NULL," +
//                "  `LASTESTOFFSET` bigint(255) NOT NULL," +
//                "  `UPTIME` varchar(100) NOT NULL" +
//                ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
//
//        MySqlUtil.getInstance().ExecuteSQL(createSql);
    }

    @Override
    public void clearPersist() {
        String deleteSql = "DELETE FROM " + tableName + " WHERE APPNAME='"+appName+"'";
        MySqlUtil.getInstance().ExecuteSQL(deleteSql);
    }

    @Override
    public boolean persistOffset(OffsetRange[] offsetRanges) {
        Date date = new Date();
        List<String> sqlList = new ArrayList<>();
        String deleteSql = "DELETE FROM " + tableName + " WHERE APPNAME='"+appName+"'";
        sqlList.add(deleteSql);
        for (int i = 0; i < offsetRanges.length; i++) {
            OffsetRange offsetRange = offsetRanges[i];
            String sql = "INSERT INTO "+tableName + " VALUES('"+appName+"','"+ offsetRange.topic()+"','"+groupId+"',"+offsetRange.partition()+","+offsetRange.untilOffset()+",'"+fastDateFormat.format(date)+"')" ;
            sqlList.add(sql);
        }
        return MySqlUtil.getInstance().ExecuteSqlList(sqlList);
    }

    @Override
    public List<TopicGroupPartitionOffset> getOffset() {
        List<TopicGroupPartitionOffset> groupTopicPartitionOffsets = new ArrayList<>();
        List<Map<String, Object>> list = MySqlUtil.getInstance().QuerySQL("SELECT * FROM "+ tableName + " WHERE APPNAME='"+ appName +"'");

        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(i);
            TopicGroupPartitionOffset tgpo = new TopicGroupPartitionOffset();
            tgpo.setGroup(groupId);
            tgpo.setTopic(map.get("TOPIC").toString());
            Integer partition =Integer.valueOf(map.get("PARTITION").toString());
            Long lastestoffset = Long.valueOf(map.get("LASTESTOFFSET").toString()) ;
            tgpo.setPartition(partition);
            tgpo.setLastestOffset(lastestoffset);
            groupTopicPartitionOffsets.add(tgpo);
        }
        return groupTopicPartitionOffsets;
    }
}
