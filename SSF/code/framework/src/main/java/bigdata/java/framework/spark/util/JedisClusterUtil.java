/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import com.typesafe.config.Config;
import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

/**
*redis集群操作工具类
 */
public class JedisClusterUtil {

    private static JedisClusterUtil instance=null;

    /**
     * 获取工具类实例对象
     */
    public static JedisClusterUtil getInstance(){
        if(instance == null){
            synchronized(JedisClusterUtil.class){
                if(instance==null){
                    instance = new JedisClusterUtil();
                }
            }
        }
        return instance;
    }

    //JedisCluster包含了所有节点的连接池
    private JedisCluster jedisCluster = null;

    private JedisClusterUtil() {
        //连接池配置
        JedisPoolConfig config = new JedisPoolConfig();
        //最大连接数
        config.setMaxTotal(20);
        //最大空闲的连接数
        config.setMaxIdle(5);
        //最小空闲的连接数
        config.setMinIdle(2);
        //当连接池用尽后，调用者的最大等待时间（单位毫秒）
        config.setMaxWaitMillis(10000);
        //向连接池借用连接时是否做连接有效性检测（ping）
        config.setTestOnBorrow(true);
        //向连接池归还连接时是否做连接有效性检测（ping）
        config.setTestOnReturn(true);
        //向连接池借用连接时是否做连接空闲检测
        config.setTestWhileIdle(true);
        //空闲连接的检测周期（单位毫秒），默认-1表示不做检测
        config.setTimeBetweenEvictionRunsMillis(30000);
        //做空闲连接检测时，每次的采样数，默认3
        config.setNumTestsPerEvictionRun(3);
        //连接的最小空闲时间，达到此值后空闲连接将被移除
        config.setMinEvictableIdleTimeMillis(60000);

        //连接超时
        int connTimeOut = 10000;
        //读写超时
        int soTimeOut = 5000;
        //重试次数
        int maxAttemts = 10;
        //集群节点列表
        Set<HostAndPort> jedisClusterNode = new HashSet<HostAndPort>();
        Config applicationConfig = ConfigUtil.config;
        if (applicationConfig.hasPath("redis.ips.ports")) {
            String redisList = applicationConfig.getString("redis.ips.ports");
            if (StringUtils.isNotEmpty(redisList)) {
                String[] hosts = redisList.split(",");
                for (String hostport : hosts) {
                    String[] hostAndport = hostport.split(":");
                    //获取IP地址
                    String ip = hostAndport[0];
                    //获取端口号
                    int port = Integer.parseInt(hostAndport[1]);

                    if (hostAndport.length != 2) {
                        throw new RuntimeException("redis.servers config error");
                    }
                    jedisClusterNode.add(new HostAndPort(ip, port));
                }

            } else {
                throw new RuntimeException("redis no config");
            }
        }

        boolean hasPWD = applicationConfig.hasPath("redis.password");
        if(hasPWD)
        {
            String pwd = applicationConfig.getString("redis.password");
            if(org.apache.commons.lang3.StringUtils.isBlank(pwd))
            {//密码为空，那么认定为没有密码
                jedisCluster = new JedisCluster(jedisClusterNode, connTimeOut, soTimeOut, maxAttemts, config);
            }
            else
            {//有密码
                jedisCluster = new JedisCluster(jedisClusterNode, connTimeOut, soTimeOut, maxAttemts,pwd, config);
            }
        }
        else
        {//配置文件中不包含redis.password，认定为没有密码
            jedisCluster = new JedisCluster(jedisClusterNode, connTimeOut, soTimeOut, maxAttemts, config);
        }
    }

    /**
     * 从连接池中获取JedisCluster对象
     */
    public JedisCluster getJedisCluster() {

        return jedisCluster;
    }
}
