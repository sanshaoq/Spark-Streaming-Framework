function trim(str)
{
    if(str == undefined)return "";
    return str.replace(/(^\s*)|(\s*$)/g, "");
}
//移除前后空格
String.prototype.trim = function () {
    return trim(this);
}

function urlParams()
{
    var urlString=location.href; //取得整个地址栏
    var urlIndex=urlString.indexOf("?")
    if(urlIndex==-1)return "";
    urlString = urlString.substr(urlIndex);
    return escape(urlString);
}

//返回上一页
function reback()
{
    var backurl = getQueryString("backurl");
    if(backurl=="")
    {
        window.history.back(-1);
    }
    else
    {
        window.location.href = backurl;
    }
}

//判断是否数字
function IsNum(num)
{
    if(num.trim()=="")return false;
    var reg = /^[0-9]*$/;
    if (reg.test(num)) {
        return true;
    }else{
        return false;
    };
}
//验证手机号码
function IsMobile(phone)
{    if(phone.trim()=="")return false;
    var reg = /^1\d{10}$/;
    if (reg.test(phone)) {
        return true;
    }else{
        return false;
    };
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return "";
}

function PrefixInteger(num, length) {
    return (Array(length).join('0') + num).slice(-length);
}
//获取url中所有参数
function getUrlParamsStr() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}