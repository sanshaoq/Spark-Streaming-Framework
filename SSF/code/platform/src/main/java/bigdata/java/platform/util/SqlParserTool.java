package bigdata.java.platform.util;

//import com.wisetv.programdept.penum.SqlType;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;

import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.util.TablesNamesFinder;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * jsqlparser解析SQL工具类
 * PlainSelect类不支持union、union all等请使用SetOperationList接口
 *
 */
public class SqlParserTool {



    /**
     * 由于jsqlparser没有获取SQL类型的原始工具，并且在下面操作时需要知道SQL类型，所以编写此工具方法
     * @param sql sql语句
     * @return sql类型，
     * @throws JSQLParserException
     */
//    public static SqlType getSqlType(String sql) throws JSQLParserException {
//        Statement sqlStmt = CCJSqlParserUtil.parse(new StringReader(sql));
//        if (sqlStmt instanceof Alter) {
//            return SqlType.ALTER;
//        } else if (sqlStmt instanceof CreateIndex) {
//            return SqlType.CREATEINDEX;
//        } else if (sqlStmt instanceof CreateTable) {
//            return SqlType.CREATETABLE;
//        } else if (sqlStmt instanceof CreateView) {
//            return SqlType.CREATEVIEW;
//        } else if (sqlStmt instanceof Delete) {
//            return SqlType.DELETE;
//        } else if (sqlStmt instanceof Drop) {
//            return SqlType.DROP;
//        } else if (sqlStmt instanceof Execute) {
//            return SqlType.EXECUTE;
//        } else if (sqlStmt instanceof Insert) {
//            return SqlType.INSERT;
//        } else if (sqlStmt instanceof Merge) {
//            return SqlType.MERGE;
//        } else if (sqlStmt instanceof Replace) {
//            return SqlType.REPLACE;
//        } else if (sqlStmt instanceof Select) {
//            return SqlType.SELECT;
//        } else if (sqlStmt instanceof Truncate) {
//            return SqlType.TRUNCATE;
//        } else if (sqlStmt instanceof Update) {
//            return SqlType.UPDATE;
//        } else if (sqlStmt instanceof Upsert) {
//            return SqlType.UPSERT;
//        } else {
//            return SqlType.NONE;
//        }
//    }

    /**
     * 获取sql操作接口,与上面类型判断结合使用
     * example:
     * String sql = "create table a(a string)";
     * SqlType sqlType = SqlParserTool.getSqlType(sql);
     * if(sqlType.equals(SqlType.SELECT)){
     *     Select statement = (Select) SqlParserTool.getStatement(sql);
     *  }
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static Statement getStatement(String sql) throws JSQLParserException {
        Statement sqlStmt = CCJSqlParserUtil.parse(new StringReader(sql));
        return sqlStmt;
    }

    /**
     * 获取tables的表名
     * @param statement
     * @return
     */
    public static List<String> getTableList(Select statement){
        TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
        List<String> tableList = tablesNamesFinder.getTableList(statement);
        return tableList;
    }

    /**
     * 获取join层级
     * @param selectBody
     * @return
     */
    public static List<Join> getJoins(SelectBody selectBody){
        if(selectBody instanceof PlainSelect){
            List<Join> joins =((PlainSelect) selectBody).getJoins();
            return joins;
        }
        return new ArrayList<Join>();
    }

    /**
     *
     * @param selectBody
     * @return
     */
    public static List<Table> getIntoTables(SelectBody selectBody){
        if(selectBody instanceof PlainSelect){
            List<Table> tables = ((PlainSelect) selectBody).getIntoTables();
            return tables;
        }
        return new ArrayList<Table>();
    }

    /**
     *
     * @param selectBody
     * @return
     */
    public static void setIntoTables(SelectBody selectBody,List<Table> tables){
        if(selectBody instanceof PlainSelect){
            ((PlainSelect) selectBody).setIntoTables(tables);
        }
    }

    /**
     * 获取limit值
     * @param selectBody
     * @return
     */
    public static Limit getLimit(SelectBody selectBody){
        if(selectBody instanceof PlainSelect){
            Limit limit = ((PlainSelect) selectBody).getLimit();
            return limit;
        }
        return null;
    }

    /**
     * 为SQL增加limit值
     * @param selectBody
     * @param l
     */
    public static void setLimit(SelectBody selectBody,long l){
        if(selectBody instanceof PlainSelect){
            Limit limit = new Limit();
            limit.setRowCount(new LongValue(String.valueOf(l)));
            ((PlainSelect) selectBody).setLimit(limit);
        }
    }

    /**
     * 获取FromItem不支持子查询操作
     * @param selectBody
     * @return
     */
    public static FromItem getFromItem(SelectBody selectBody){
        if(selectBody instanceof PlainSelect){
            FromItem fromItem = ((PlainSelect) selectBody).getFromItem();
            return fromItem;
        }else if(selectBody instanceof WithItem){
            SqlParserTool.getFromItem(((WithItem) selectBody).getSelectBody());
        }
        return null;
    }

    /**
     * 获取子查询
     * @param selectBody
     * @return
     */
    public static SubSelect getSubSelect(SelectBody selectBody){
        if(selectBody instanceof PlainSelect){
            FromItem fromItem = ((PlainSelect) selectBody).getFromItem();
            if(fromItem instanceof SubSelect){
                return ((SubSelect) fromItem);
            }
        }else if(selectBody instanceof WithItem){
            SqlParserTool.getSubSelect(((WithItem) selectBody).getSelectBody());
        }
        return null;
    }

    /**
     * 判断是否为多级子查询
     * @param selectBody
     * @return
     */
    public static boolean isMultiSubSelect(SelectBody selectBody){
        if(selectBody instanceof PlainSelect){
            FromItem fromItem = ((PlainSelect) selectBody).getFromItem();
            if(fromItem instanceof SubSelect){
                SelectBody subBody = ((SubSelect) fromItem).getSelectBody();
                if(subBody instanceof PlainSelect){
                    FromItem subFromItem = ((PlainSelect) subBody).getFromItem();
                    if(subFromItem instanceof SubSelect){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 获取查询字段
     * @param selectBody
     * @return
     */
    public static List<SelectItem> getSelectItems(SelectBody selectBody){
        if(selectBody instanceof PlainSelect){
            List<SelectItem> selectItems = ((PlainSelect) selectBody).getSelectItems();
            return selectItems;
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
//        Statement sqlStmt = CCJSqlParserUtil.parse(new StringReader("show databases"));

//         String sql = "select t1.a,t2.b from table1 t1 left join table2 t2 on t1.a = t2.b";
//        String sql = "select * from (select t1.a,t2.b from table1 t1 left join table2 t2 on t1.a = t2.b) a";
        String sql = "select t1.a,t2.b from table1 t1 left join table2 t2 on t1.a = t2.b left join table3 t3 on t2.c=t3.d where t1.a = ?";
        //String sql = "select t1.a,t2.b from table1 as t1,table2 as t2,table3 t3 where t1.a = t2.b and t2.c=t3.d and t1.a = ?";
//        String sql = "select t1 from table1 t1 union all select t2.b,t3.c from table2 t2,table3 t3 where t2.b = t3.c";
        getTableRelation(sql);
    }
    static Map<String, String> tableMap = new HashMap<>();
    public static void getTableRelation(String sql) throws Exception {
        Statement stmt = CCJSqlParserUtil.parse(sql);
        if (stmt instanceof Select) {
            Select select = (Select) stmt;
            SelectBody selectBody = select.getSelectBody();
            alise(selectBody);
        }
    }

    private static void alise(SelectBody selectBody) {
        if (selectBody instanceof PlainSelect) {
            PlainSelect plainSelect = (PlainSelect) selectBody;
            FromItem fromItem = plainSelect.getFromItem();
            List<Join> joins = plainSelect.getJoins();

            if(joins != null) {
                for (Join join : joins) {
                    Expression onExpression = join.getOnExpression();
                    FromItem rightItem = join.getRightItem();
                    if (rightItem instanceof Table) {
                        Table table = (Table) rightItem;
                        tableMap.put(table.getAlias().getName(), table.getName());
                    }
                    doExpression(onExpression);
                }
            }

            if (fromItem instanceof Table) {
                Table table = (Table) fromItem;
                tableMap.put(table.getAlias().getName(), table.getName());
            }else if(fromItem instanceof SubSelect) {
                SubSelect subSelect = (SubSelect) fromItem;
                SelectBody subSelectBody = subSelect.getSelectBody();
                alise(subSelectBody);
            }

            doExpression(plainSelect.getWhere());
        }else if(selectBody instanceof SetOperationList) {
            SetOperationList setOperationList = (SetOperationList) selectBody;
            List<SelectBody> selects = setOperationList.getSelects();
            for (SelectBody selectBody3 : selects) {
                alise(selectBody3);
            }
        }
    }

    private static void doExpression(Expression expression) {
        if (expression instanceof EqualsTo) {
            EqualsTo equalsTo = (EqualsTo) expression;
            Expression rightExpression = equalsTo.getRightExpression();
            Expression leftExpression = equalsTo.getLeftExpression();
            if (rightExpression instanceof Column && leftExpression instanceof Column) {
                Column rightColumn = (Column) rightExpression;
                Column leftColumn = (Column) leftExpression;
                System.out.println(tableMap.get(rightColumn.getTable().toString()) + "表的" + rightColumn.getColumnName() + "字段 -> "
                        + tableMap.get(leftColumn.getTable().toString()) + "表的" + leftColumn.getColumnName() + "字段");
            }
        }else if(expression instanceof AndExpression){
            AndExpression andExpression = (AndExpression) expression;
            Expression leftExpression = andExpression.getLeftExpression();
            doExpression(leftExpression);
            Expression rightExpression = andExpression.getRightExpression();
            doExpression(rightExpression);
        }
    }

}
