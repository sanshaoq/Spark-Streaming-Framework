package bigdata.java.platform.service.impl;

import bigdata.java.platform.beans.*;
import bigdata.java.platform.service.*;
import bigdata.java.platform.util.Comm;
import bigdata.java.platform.util.DateUtil;
import bigdata.java.platform.util.HBaseUtil;
import bigdata.java.platform.util.KafkaTools;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import kafka.common.TopicAndPartition;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scala.Tuple5;

import java.util.*;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    SystemService systemService;

    @Autowired
    SparkJobService sparkJobService;

    @Autowired
    TaskLogService taskLogService;

    @Autowired
    UserService userService;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public TTask getDefault(Integer userId) {

        SysSet sysSet = systemService.get(userId);
        TTask tTask = new TTask();
        tTask.setConf(sysSet.getConf());
        tTask.setParam(sysSet.getParam());
        tTask.setJars(sysSet.getDefaultJars());
        tTask.setFiles(sysSet.getDefaultFiles());
        tTask.setArgs(sysSet.getArgs());
        List<SparkJob> list = sparkJobService.list(Comm.getUserId(),10);
        tTask.setJobList(list);
        return tTask;
    }
    @Override
    public List<Map<String, Object>> getTotalMetrics(Integer userId)
    {
        String sql="SELECT status,count(status) as count FROM `Task` where userId=:userId and status in (10,20) group by status";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId",userId);
        List<Map<String, Object>> list = namedParameterJdbcTemplate.queryForList(sql, parameters);
        return list;
    }
    @Override
    public Map<String, Object> getTotalNumberMetrics(Integer userId)
    {
//        String sql="select tl.waitingBatches,tl.totalCompletedBatches,tl.totalProcessedRecords from Task as t left join TaskLog as tl on t.taskid = tl.taskid where tl.applicationid is not null and tl.waitingBatches is not null";
        String sql="select sum(tl.totalCompletedBatches) as totalCompletedBatches,sum(tl.totalProcessedRecords) as totalProcessedRecords from Task as t left join TaskLog as tl on t.taskid = tl.taskid where t.userId="+userId+" and tl.applicationid is not null and tl.waitingBatches is not null";
        Map<String, Object> map = namedParameterJdbcTemplate.getJdbcTemplate().queryForMap(sql);
        return map;
    }
    @Override
    public List<Map<String, Object>> getWaitingBatches(Integer userId)
    {
        String sql="select t.taskname, tl.tasklogid,tl.taskid,tl.waitingBatches from (select * from Task where userId=:userId  and  status=20) as t left join (select * from TaskLog where tasklogid in (select max(tasklogid) from TaskLog group by taskid)) as tl on t.taskid = tl.taskid";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId",userId);
        List<Map<String, Object>> list = namedParameterJdbcTemplate.queryForList(sql, parameters);
        return list;
    }

    //select  tl.tasklogid,tl.taskid,tl.waitingBatches from
    //(select * from Task where status=20) as t left join
    //(select * from TaskLog where tasklogid in (select max(tasklogid) from TaskLog group by taskid)) as tl
    //	on t.taskid = tl.taskid

    @Override
    public TTask get(Integer userId,Integer taskId) {
        String sql = "select * from Task where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        TTask tTask = namedParameterJdbcTemplate.queryForObject(sql, parameters, TTask.class);
        List<SparkJob> list = sparkJobService.list(userId,10);
        tTask.setJobList(list);

        List<TaskLog> taskLogs = taskLogService.listForGroupMaxId();
        for (int j = 0; j < taskLogs.size(); j++) {
            TaskLog taskLog = taskLogs.get(j);
            if(tTask.getTaskId().intValue() == taskLog.getTaskId().intValue())
            {
                tTask.setTaskLog(taskLog);
                break;
            }
        }
        return tTask;
    }

    @Override
    public TTask get(Integer taskId)
    {
        return get(Comm.getUserId(),taskId);
    }

    @Override
    public void updateQueueName(Integer userId,String queueName)
    {
        String sql = "update Task set queueName=:queueName  where userId=:userId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId",userId);
        parameters.addValue("queueName",queueName);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
    }

    @Override
    public TTask getByAppName(String appName) {
        String sql = "select * from Task where appName=:appName";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("appName",appName);
        TTask tTask = namedParameterJdbcTemplate.queryForObject(sql, parameters, TTask.class);
        if(tTask==null)
        {
            return null;
        }

        List<SparkJob> list = sparkJobService.list(tTask.getUserId(),10);

        tTask.setJobList(list);

        List<TaskLog> taskLogs = taskLogService.listForGroupMaxId();
        for (int j = 0; j < taskLogs.size(); j++) {
            TaskLog taskLog = taskLogs.get(j);
            if(tTask.getTaskId().intValue() == taskLog.getTaskId().intValue())
            {
                tTask.setTaskLog(taskLog);
                break;
            }
        }
        return tTask;
    }

    @Override
    public List<TTask> list(Integer userId) {
        String sql="";
        List<TTask> query = new ArrayList<>();
        if(userId !=null)
        {
            sql="select  t.*,ui.warnPhone from Task as  t left join UserInfo as ui on t.userId = ui.UserId where t.userId=:userId";
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("userId",userId);
            query = namedParameterJdbcTemplate.query(sql,parameters, new BeanPropertyRowMapper<>(TTask.class));
        }
        else
        {
            sql="select  t.*,ui.warnPhone from Task as  t left join UserInfo as ui on t.userId = ui.UserId";
            query = namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TTask.class));
        }
        List<TaskLog> taskLogs = taskLogService.listForGroupMaxId();

        //----------------

        List<SparkJob> list = sparkJobService.list(userId,10);

        for (int i = 0; i < query.size(); i++) {
            TTask tTask = query.get(i);
            tTask.setJobList(list);
            for (int j = 0; j < taskLogs.size(); j++) {
                TaskLog taskLog = taskLogs.get(j);
                if(tTask.getTaskId().intValue() == taskLog.getTaskId().intValue())
                {
                    tTask.setTaskLog(taskLog);
                    break;
                }
            }
        }
        return query;
    }


    @Override
    public List<TTask> list() {
        String sql="";
        List<TTask> query = new ArrayList<>();

        sql="select  t.*,ui.warnPhone from Task as  t left join UserInfo as ui on t.userId = ui.UserId";
        query = namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TTask.class));

        List<TaskLog> taskLogs = taskLogService.listForGroupMaxId();

        //List<SparkJob> list = sparkJobService.list(userId,10);
        List<SparkJob> list = sparkJobService.listAllUser(-1);

        for (int i = 0; i < query.size(); i++) {
            TTask tTask = query.get(i);
            tTask.setJobList(list);
            for (int j = 0; j < taskLogs.size(); j++) {
                TaskLog taskLog = taskLogs.get(j);
                if(tTask.getTaskId().intValue() == taskLog.getTaskId().intValue())
                {
                    tTask.setTaskLog(taskLog);
                    break;
                }
            }
        }
        return query;
    }

    @Override
    public List<TTask> listByStatus(Integer userId,Integer status) {
        String sql="select * from Task where  userId=:userId and status=:status";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("status",status);
        parameters.addValue("userId",userId);
        List<TTask> query = namedParameterJdbcTemplate.query(sql,parameters, new BeanPropertyRowMapper<>(TTask.class));

        List<SparkJob> list = sparkJobService.list(userId,10);
        List<TaskLog> taskLogs = taskLogService.listForGroupMaxId();
        for (int i = 0; i < query.size(); i++) {
            TTask tTask = query.get(i);
            tTask.setJobList(list);
            for (int j = 0; j < taskLogs.size(); j++) {
                TaskLog taskLog = taskLogs.get(j);
                if(tTask.getTaskId().intValue() == taskLog.getTaskId().intValue())
                {
                    tTask.setTaskLog(taskLog);
                }
            }
        }

        return query;
    }

    @Transactional
    @Override
    public Boolean delete(Integer taskId) {
        String sql = "delete from Task  where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        taskLogService.deleteByTaskId(taskId);
        return update > 0;
    }

    @Override
    public Boolean edit(TTask tTask) {
        String sql = "update Task set taskType=:taskType,appName=:appName,taskName=:taskName,classFullName=:classFullName,status=:status,updateTime=:updateTime,conf=:conf,args=:args,jars=:jars,files=:files,param=:param,jobId=:jobId,queueName=:queueName,activeBatch=:activeBatch,restartActiveBatch=:restartActiveBatch,openRestartActiveBatch=:openRestartActiveBatch,webApi=:webApi,webApiToken=:webApiToken,openChart=:openChart  where taskId=:taskId";
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(tTask);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Transactional
    @Override
    public Boolean add(TTask tTask) {
        String sql = "insert into Task (taskType,appName,taskName,classFullName,status,updateTime,conf,args,jars,files,param,userId,jobId,queueName,activeBatch,restartActiveBatch,openRestartActiveBatch,webApi,webApiToken,openChart)values(:taskType,:appName,:taskName,:classFullName,:status,:updateTime,:conf,:args,:jars,:files,:param,:userId,:jobId,:queueName,:activeBatch,:restartActiveBatch,:openRestartActiveBatch,:webApi,:webApiToken,:openChart)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(tTask);
        int update = namedParameterJdbcTemplate.update(sql, parameters,keyHolder,new String[]{"taskId"});
        tTask.setTaskId(keyHolder.getKey().intValue());
        taskLogService.addTaskId(tTask.getTaskId(),tTask.getUpdateTime());
        return update > 0;
    }

    @Override
    public Boolean updateErrCount(Integer taskId, Integer errCount) {
        return updateErrCount(taskId,errCount,new Date());
    }

    @Override
    public Boolean updateErrCount(Integer taskId, Integer errCount, Date updateTime) {
        String sql = "update Task set errCount=:errCount,updateTime=:updateTime where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("errCount",errCount);
        parameters.addValue("updateTime",updateTime);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Transactional
    @Override
    public Boolean setStatusAndClearErrCountAndClearTryCount(Integer taskId, Integer status, Date updateTime) {
        //start--------2019年9月2日优雅关闭修改(新增代码)
        if(status == TTask.STOPING)
        {
            TTask tTask = get(taskId);
            if(!StringUtils.isBlank(tTask.getSaveType()))
            {
                if(tTask.getSaveType().equals("MySql"))
                {
                    stopByMarkKey_Mysql(tTask.getAppName());
                }
                else
                {
                    stopByMarkKey_Hbase(tTask.getAppName());
                }
            }
        }
        //start--------2019年9月2日优雅关闭修改(新增代码)

        String sql = "update Task set status=:status,updateTime=:updateTime,errCount=0,tryCount=0 where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("status",status);
        parameters.addValue("updateTime",updateTime);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public void stopByMarkKey_Mysql(String appname)
    {
        String formateTime = DateUtil.getFormateTime(DateUtil.FORMAT_YYYY_MM_DDHHMMSS);
        String sql = "update STOPBYMARK set FLAG=:FLAG,UPTIME=:UPTIME where APPNAME=:APPNAME";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("APPNAME",appname);
        parameters.addValue("FLAG",0);
        parameters.addValue("UPTIME",formateTime);
        namedParameterJdbcTemplate.update(sql, parameters);
    }
    @Override
    public void stopByMarkKey_Hbase(String appname)
    {
        String formateTime = DateUtil.getFormateTime(DateUtil.FORMAT_YYYY_MM_DDHHMMSS);
        Map<String, String> columnAndValues = new HashMap<>();
        columnAndValues.put("FLAG","0");
        columnAndValues.put("UPTIME",formateTime);
        HBaseUtil.put("SPARKSTREAMING:STOPBYMARK",appname,"F",columnAndValues);
    }

    public void deleteStopbyMark_Mysql(String appName)
    {
        String sql = "delete from STOPBYMARK where APPNAME=:APPNAME";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("APPNAME",appName);
        namedParameterJdbcTemplate.update(sql, parameters);
    }

    public void deleteStopbyMark_Hbase(String appName)
    {
        HBaseUtil.deleteRow("SPARKSTREAMING:STOPBYMARK",appName);
    }

    @Transactional
    @Override
    public Boolean setStatusAndClearErrCountAndSetTryCount(Integer taskId, Integer status,Integer tryCount, Date updateTime) {
        String sql = "update Task set status=:status,errCount=0,tryCount=:tryCount,updateTime=:updateTime where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("status",status);
        parameters.addValue("tryCount",tryCount);
        parameters.addValue("updateTime",updateTime);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    public Boolean updateTryCount(Integer taskId, Integer tryCount, Date updateTime)
    {
        String sql = "update Task set tryCount=:tryCount,updateTime=:updateTime where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("tryCount",tryCount);
        parameters.addValue("updateTime",updateTime);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public Boolean updateStartTime(Integer taskId,Date startTime)
    {
        String sql = "update Task set startTime=:startTime where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("startTime",startTime);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public Boolean updateOffSet(Integer taskId,String topic,String partition,String currentOffset,String min,String max)
    {
        TTask tTask = get(taskId);
        String formateTime = DateUtil.getFormateTime(DateUtil.FORMAT_YYYY_MM_DDHHMMSS);
        if("MySql".equals(tTask.getSaveType()))
        {
            String sql = "select count(*) as c from platform.KAFKAOFFSET where APPNAME=:APPNAME and TOPIC=:TOPIC and GROUPID=:GROUPID and `PARTITION`=:PARTITION";
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("APPNAME",tTask.getAppName());
            parameters.addValue("TOPIC",topic);
            parameters.addValue("GROUPID",tTask.getAppName());
            parameters.addValue("PARTITION",partition);
            Map<String, Object> map = new HashMap<>();

            map = namedParameterJdbcTemplate.queryForMap(sql, parameters);
            int count = Integer.parseInt(map.get("c").toString());

            if(count > 0)
            {
                sql = "update platform.KAFKAOFFSET set LASTESTOFFSET=:LASTESTOFFSET,UPTIME=:UPTIME where APPNAME=:APPNAME and TOPIC=:TOPIC and GROUPID=:GROUPID and `PARTITION`=:PARTITION";
            }
            else
            {
                sql ="insert into platform.KAFKAOFFSET (APPNAME,TOPIC,GROUPID,`PARTITION`,LASTESTOFFSET,UPTIME)value(:APPNAME,:TOPIC,:GROUPID,:PARTITION,:LASTESTOFFSET,:UPTIME)";
            }

            parameters.addValue("UPTIME",formateTime);
            parameters.addValue("LASTESTOFFSET",currentOffset);
            namedParameterJdbcTemplate.update(sql,parameters);
        }
        else if("Hbase".equals(tTask.getSaveType()))
        {
            String json = HBaseUtil.get("SPARKSTREAMING:KAFKAOFFSET", tTask.getAppName(), "F", "JSON");
            JSONArray jsonArray = new JSONArray();
            if(StringUtils.isBlank(json))
            {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("TOPIC",topic);
                jsonObject.put("GROUPID",tTask.getAppName());
                jsonObject.put("PARTITION",partition);
                jsonObject.put("LASTESTOFFSET",currentOffset);
                jsonObject.put("UPTIME",formateTime);
                jsonArray.add(jsonObject);
            }
            else
            {
                jsonArray = JSONObject.parseArray(json);
                boolean find= false;
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String topic1 = jsonObject.getString("TOPIC");
                    String groupid2 = jsonObject.getString("GROUPID");
                    String partition2 = jsonObject.getString("PARTITION");
                    String lastestoffset2 = jsonObject.getString("LASTESTOFFSET");
                    if(topic.equals(topic1) && tTask.getAppName().equals(groupid2)&& partition.equals(partition2))
                    {
                        jsonObject.put("LASTESTOFFSET",currentOffset);
                        jsonObject.put("UPTIME",formateTime);
                        find =true;
                        break;
                    }
                }
                if(!find)
                {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("TOPIC",topic);
                    jsonObject.put("GROUPID",tTask.getAppName());
                    jsonObject.put("PARTITION",partition);
                    jsonObject.put("LASTESTOFFSET",currentOffset);
                    jsonObject.put("UPTIME",formateTime);
                    jsonArray.add(jsonObject);
                }
            }
            HBaseUtil.put("SPARKSTREAMING:KAFKAOFFSET",tTask.getAppName(),"F","JSON",jsonArray.toJSONString());
        }
        return true;
    }

    @Override
    public List<KafkaOffset> getOffset(Integer userId,Integer taskId)
    {
        TTask tTask = get(userId,taskId);
        String saveType = tTask.getSaveType();
        //"topics:yxgk.a_Cashchk_Flow,yxgk.A_Pay_Flow","duration:2","maxnum:500"
        String args = tTask.getArgs();
        //解析topic
        String topics="";
        String[] splits = args.split("\",\"");
        for (int i = 0; i < splits.length; i++) {
            String split = splits[i];
            if(split.contains("topics:"))
            {
                String[] split1 = split.split(":");
                topics = split1[1].replace("\"","");
                break;
            }
        }


        List<KafkaOffset> tupleList = new ArrayList<>();
        List<Map<String, Object>> list = new ArrayList<>();
        if("MySql".equals(saveType))
        {
            String sql = "select * from platform.KAFKAOFFSET where APPNAME=:appname";
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("appname",tTask.getAppName());
            //mysql数据中保存的offset
            list = namedParameterJdbcTemplate.queryForList(sql, parameters);
        }
        else if("Hbase".equals(saveType))
        {
            String json = HBaseUtil.get("SPARKSTREAMING:KAFKAOFFSET", tTask.getAppName(), "F", "JSON");
            if(!StringUtils.isBlank(json))
            {
                JSONArray jsonArray = JSONObject.parseArray(json);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String dbTOPIC = jsonObject.getString("TOPIC");
                    String dbPARTITION = jsonObject.getString("PARTITION");
                    String dbLASTESTOFFSET = jsonObject.getString("LASTESTOFFSET");
                    Map<String,Object> map = new HashMap<>();
                    map.put("TOPIC",dbTOPIC);
                    map.put("PARTITION",dbPARTITION);
                    map.put("LASTESTOFFSET",dbLASTESTOFFSET);
                    list.add(map);
                }
            }
        }
        KafkaOffset kafkaOffset = KafkaTools.getKafkaOffset(topics);
        Map<TopicAndPartition, Long> lastOffsetMap = kafkaOffset.getLastOffsetMap();
        Map<TopicAndPartition, Long> earliestOffsetMap = kafkaOffset.getEarliestOffsetMap();
        for (Map.Entry<TopicAndPartition, Long> map : lastOffsetMap.entrySet())
        {
                String topic = map.getKey().topic();
                String partition = String.valueOf(map.getKey().partition());
                String maxOffset = map.getValue().toString();

                KafkaOffset offset = new KafkaOffset();
                offset.setTopic(topic);
                offset.setPartition(partition);
                offset.setMaxOffset(maxOffset);
                tupleList.add(offset);
        }

        for (int i = 0; i < tupleList.size(); i++) {
            KafkaOffset offset = tupleList.get(i);
            String topic = offset.getTopic();
            String partition = String.valueOf(offset.getPartition());

            for (Map.Entry<TopicAndPartition, Long> map : earliestOffsetMap.entrySet())
            {
                String topic2 = map.getKey().topic();
                String partition2 = String.valueOf(map.getKey().partition());
                String minOffset = map.getValue().toString();
                if(topic.equals(topic2) && partition.equals(partition2))
                {
                    offset.setMinOffset(minOffset);
                }
            }

            for (int j = 0; j < list.size(); j++) {
                Map<String, Object> map = list.get(j);
                String dbTOPIC = map.get("TOPIC").toString();
                String dbPARTITION = map.get("PARTITION").toString();
                String dbLASTESTOFFSET = map.get("LASTESTOFFSET").toString();
                if(topic.equals(dbTOPIC) && dbPARTITION.equals(partition))
                {
                    offset.setCurrentOffset(dbLASTESTOFFSET);
                }
            }
        }

        return tupleList;

    }

    @Override
    public Boolean updateErrCountAndTryCount(Integer taskId, Integer errCount, Integer tryCount, Date date) {
        String sql = "update Task set errCount=:errCount,tryCount=:tryCount,updateTime=:updateTime where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("errCount",errCount);
        parameters.addValue("tryCount",tryCount);
        parameters.addValue("updateTime",date);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public Boolean setStatus(Integer taskId , Integer status, Date date) {
        String sql = "update Task set status=:status,updateTime=:updateTime where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("status",status);
        parameters.addValue("updateTime",date);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public void clearAllTaskErrCountAndClearTryCount() {
        String sql = "update Task set errCount=0,tryCount=0";
        namedParameterJdbcTemplate.getJdbcTemplate().update(sql);
    }
}
