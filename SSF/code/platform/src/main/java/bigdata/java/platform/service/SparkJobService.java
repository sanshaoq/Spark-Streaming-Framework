package bigdata.java.platform.service;

import bigdata.java.platform.beans.SparkJob;
import bigdata.java.platform.beans.TFile;

import java.util.List;

public interface SparkJobService {
    Boolean add(SparkJob sparkJob);
    Boolean delete(Integer jobId);
    List<SparkJob> list(Integer userId,Integer limit);
    Integer maxJobId(Integer userId);
    List<SparkJob> list(Integer userId);
    List<SparkJob> listAllUser(Integer limit);
    SparkJob get(Integer jobId);
    Boolean setDefaultJob(Integer jobId);
}
