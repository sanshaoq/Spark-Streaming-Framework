package bigdata.java.platform.beans;

import bigdata.java.platform.util.Comm;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1690312405427149354L;
    Integer userId;
    String nickName;
    String userName;
    String passWord;
    String isAdmin;
    String warnPhone;
    public String getWarnPhone() {
        return warnPhone;
    }

    String queueName;

    public String getQueueName() {
            return queueName;
    }

    public String getQueueNameOrMap() {
        String quaue_name = Comm.userQuaueNameMap.get(userId);
        if(StringUtils.isBlank(quaue_name))
        {
            return queueName;
        }
        else
        {
            return quaue_name;
        }
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public void setWarnPhone(String warnPhone) {
        this.warnPhone = warnPhone;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }



    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}